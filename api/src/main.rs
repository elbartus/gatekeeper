extern crate pretty_env_logger;
#[macro_use]
extern crate gatekeeper_derive;

use serde_derive::Serialize;
use std::convert::Infallible;
use warp::http::StatusCode;
use warp::{reject, Filter, Rejection, Reply};

pub trait RejectDetail {
    fn get_detail(&self) -> String;
}

#[derive(Debug, RejectDetail)]
struct AuthorizationHeaderMissing {
    detail: String,
}

#[derive(Debug, RejectDetail)]
struct CockpitUserCookieMissing {
    detail: String,
}

#[derive(Debug, RejectDetail)]
struct AuthorizationFailed {
    detail: String,
}

impl reject::Reject for AuthorizationHeaderMissing {}
impl reject::Reject for CockpitUserCookieMissing {}
impl reject::Reject for AuthorizationFailed {}

/// An API error serializable to JSON.
#[derive(Serialize)]
struct ErrorMessage {
    code: u16,
    message: String,
}

#[derive(Debug, Serialize)]
struct User {
    name: String,
}

fn validate_authorization_header(header_string: String) -> Result<User, Rejection> {
    if !header_string.starts_with("Signature") {
        return Err(reject::custom(AuthorizationFailed {
            detail: "Unsupported Authorization method.".to_string(),
        }));
    }

    todo!()
}

fn api_sec_auth() -> impl Filter<Extract = (User,), Error = Rejection> + Copy {
    warp::header::optional::<String>("authorization").and_then(
        |auth_header: Option<String>| async move {
            if let Some(auth_header) = auth_header {
                match validate_authorization_header(auth_header) {
                    Ok(u) => Ok(u),
                    Err(r) => Err(r),
                }
            // if let Ok(u) = validate_authorization_header(auth_header) {
            //     Ok(u)
            // } else {
            //     Err(reject::custom(AuthorizationFailed {
            //         detail: "Authorization from Authorization header failed.".to_string(),
            //     }))
            // }
            } else {
                Err(reject::custom(AuthorizationHeaderMissing {
                    detail: "Authorization header missing!".to_string(),
                }))
            }
        },
    )
}

fn cookie_auth() -> impl Filter<Extract = (User,), Error = Rejection> + Copy {
    warp::header::optional::<String>("cookie").and_then(
        |cookie_header: Option<String>| async move {
            if let Some(cookies) = cookie_header {
                if cookies.contains("elbart") {
                    Ok(User {
                        name: "elbart from cookie".to_string(),
                    })
                } else {
                    Err(reject::custom(AuthorizationFailed {
                        detail: "Cockpit authorization from Cookies failed.".to_string(),
                    }))
                }
            } else {
                Err(reject::custom(CockpitUserCookieMissing {
                    detail: "Cockpit auth information missing from Cookie header.".to_string(),
                }))
            }
        },
    )
}

async fn handle_errors(err: Rejection) -> Result<impl Reply, Infallible> {
    let code;
    let message;

    if let Some(ex) = err.find::<AuthorizationFailed>() {
        code = StatusCode::FORBIDDEN;
        message = ex.detail.clone();
    } else if let Some(ex) = err.find::<AuthorizationHeaderMissing>() {
        if let Some(ex) = err.find::<CockpitUserCookieMissing>() {
            code = StatusCode::UNAUTHORIZED;
            message = ex.detail.clone()
        } else {
            code = StatusCode::UNAUTHORIZED;
            message = ex.detail.clone();
        }
    } else if let Some(header) = err.find::<warp::reject::MissingHeader>() {
        code = StatusCode::BAD_REQUEST;
        message = format!("Missing header '{}'", header.name());
    } else {
        // We should have expected this... Just log and say its a 500
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = "Unhandled error".to_string();
    }

    let json = warp::reply::json(&ErrorMessage {
        code: code.as_u16(),
        message,
    });

    Ok(warp::reply::with_status(json, code))
}

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let access_log = warp::log::log("access");
    // GET /hello/warp => 200 OK with body "Hello, warp!"
    let ngx_auth = warp::path("api")
        .and(warp::path("ngx-auth"))
        .and(api_sec_auth())
        .or(cookie_auth())
        .unify()
        .map(|user: User| format!("{:#?}", user));

    let routes = ngx_auth.recover(handle_errors).with(access_log);

    warp::serve(routes).run(([127, 0, 0, 1], 7117)).await;
}
