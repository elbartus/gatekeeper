# Nginx build

## Build process

Install dependencies:

For apt:
```
sudo apt install libpcre3-dev
```

Run build-script:

```
./build.sh
```

Start nginx:

```
./run.sh