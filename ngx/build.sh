#!/bin/bash

set -xe

BASEDIR=$(pwd)
TARGETDIR=${BASEDIR}/target
DOWNLOADDIR=${BASEDIR}/download
NGINX_VERSION="1.18.0"

rm -rf ${DOWNLOADDIR} && mkdir ${DOWNLOADDIR}
rm -rf ${TARGETDIR} && mkdir ${TARGETDIR}
wget --directory-prefix=${DOWNLOADDIR} http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
cd ${DOWNLOADDIR}
tar xzvf nginx-${NGINX_VERSION}.tar.gz

curl -L https://api.github.com/repos/openresty/echo-nginx-module/tarball > ${DOWNLOADDIR}/echo-nginx-module.tar.gz
mkdir echo-nginx-module
tar -xzvf ${DOWNLOADDIR}/echo-nginx-module.tar.gz --strip-components=1 -C echo-nginx-module

# exit
cd ${DOWNLOADDIR}/nginx-${NGINX_VERSION}
./configure \
    --prefix=${TARGETDIR} \
    --with-http_auth_request_module \
    --with-http_ssl_module \
    --with-http_v2_module \
    --with-http_realip_module \
    --add-module=${DOWNLOADDIR}/echo-nginx-module

make -j4
make install

cd ${BASEDIR}
cp -r etc/* ${TARGETDIR}/conf/
${TARGETDIR}/sbin/nginx -t