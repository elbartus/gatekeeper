#!/bin/bash

BASEDIR=$(pwd)
TARGETDIR=${BASEDIR}/target

cp -r etc/* ${TARGETDIR}/conf/
${TARGETDIR}/sbin/nginx -t
${TARGETDIR}/sbin/nginx