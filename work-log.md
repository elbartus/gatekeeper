# 2020-07-29
## Goals
- Create an /auth endpoint which returns an HTTP 200
- Compile nginx locally with ngx_http_auth_request_module
- Create nginx config and let it run with the gatekeeper once

## Useful resources
- How to employ nginx auth request module: https://developer.okta.com/blog/2018/08/28/nginx-auth-request
- Warp HTTP webserver: https://github.com/seanmonstar/warp
- Vouch Proxy: https://github.com/vouch/vouch-proxy/
- Also for nginx auth request module: https://redbyte.eu/en/blog/using-the-nginx-auth-request-module/

## Ideas for next time
- Define headers to forward from the original request, which are not in headers
- build first HTTP 200/401/403 handling in warp

# 2020-07-31
## Goals
- Define headers to be forwarded by nginx to the upstream ngx-auth endpoint
- read about warp rejections and implement 200 vs 401 handling

## Doing
### Header Defition
- X-Forwarded-For: List of IPs + original IP
- X-Forwarded-Proto: original protocol - can be http or https
- X-Forwarded-Host: original host header requested by the client
- X-Forwarded-URI: full path+query params from the request
- X-Forwarded-Method: original request method (GET,POST,PUT, ...)
### Distinction

## Useful resources
- nginx variable index: http://nginx.org/en/docs/varindex.html

## Ideas for next time
- Implement API Security from Authentication header

# 2020-08-02
## Doing
- Add derive proc macro for Custom Errors

## Useful resources
- https://tools.ietf.org/html/draft-cavage-http-signatures-12#section-2.3
- https://docs.rs/http-signatures/0.8.0/http_signatures/
- https://doc.rust-lang.org/reference/procedural-macros.html

## Next time
- finish derive macros for custom errors + output those to the client

# 2020-08-04
## Doing
- Finished derive macros for custom errors
- 